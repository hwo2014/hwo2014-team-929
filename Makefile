CXX = g++
CXXFLAGS = -std=c++11 -g

DEFAULT_EXECNAME = vormulaBot

OBJDIR = obj
SRCDIR = src

SOURCES := $(shell find $(SRCDIR) -name '*.cpp')
DEPS := $(shell find $(SRCDIR) -name '*.h')
OBJS := $(patsubst $(SRCDIR)/%.cpp,$(OBJDIR)/%.o,$(SOURCES))

INCDIRS := $(SRCDIR)/jsoncons/src# $(HOME)/boost-1.54.0/include
LIBDIRS := #$(HOME)/boost-1.54.0/lib
LIBS := pthread boost_system

INCFLAGS := $(foreach includedir,$(INCDIRS),-I$(includedir))
LDFLAGS += $(foreach librarydir,$(LIBDIRS),-L$(librarydir))
LDFLAGS += $(foreach library,$(LIBS),-l$(library))

ifndef MKDIR
	MKDIR := mkdir -p
endif

ifndef EXECNAME
	EXECNAME := $(DEFAULT_EXECNAME)
endif

.PHONY: all clean distclean show dirs

all: $(EXECNAME) 

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp $(DEPS)
	$(CXX) $(CXXFLAGS) $(INCFLAGS) -c $< -o $@

$(EXECNAME): dirs $(OBJS)
	$(CXX) $(LDFLAGS) $(OBJS) -o $(EXECNAME)

dirs:
	$(MKDIR) obj

clean:
	- $(RM) $(OBJS)

distclean: clean
	- $(RM) $(EXECNAME)

show:
	@echo 'CXX      :' $(CXX)
	@echo 'CXXFLAGS :' $(CXXFLAGS)
	@echo 'EXECNAME :' $(EXECNAME)
	@echo 'OBJDIR   :' $(OBJDIR)
	@echo 'SRCDIR   :' $(SRCDIR)
	@echo 'SOURCES  :' $(SOURCES)
	@echo 'DEPS     :' $(DEPS)
	@echo 'OBJS     :' $(OBJS)
	@echo 'INCDIRS  :' $(INCDIRS)
	@echo 'LIBDIRS  :' $(LIBDIRS)
	@echo 'LIBS     :' $(LIBS)
	@echo 'LDFLAGS  :' $(LDFLAGS)
	@echo 'INCFLAGS :' $(INCFLAGS)
	@echo 'MKDIR    :' $(MKDIR)
	@echo 'RM       :' $(RM)