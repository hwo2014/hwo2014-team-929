#include "governor.h"

#include "track_info.h"
#include "state_PODs.h"

#include <algorithm>

void VelocityGovernor::start()
{
    _running = true;
    std::cout << "Governor: started" << std::endl;
}
void VelocityGovernor::stop()
{
    _running = false;
    std::cout << "Governor: stopped" << std::endl;
}

void VelocityGovernor::readTrack()
{
    _targetVel.resize(_trackInfo.numPieces());
    for (auto& vec : _targetVel)
    {
        vec.resize(_trackInfo.numLanes());
    }
    _route.resize(_trackInfo.numPieces());
    std::fill(_route.begin(), _route.end(), 0);
}

void VelocityGovernor::recalculate(CarState const& state)
{
    for (int piece = 0; piece < _trackInfo.numPieces(); ++piece)
    {
        if (_trackInfo.piece(piece).type() == TrackPiece::Type::Straight)
        {
            for (auto& vel : _targetVel[piece])
            {
                vel = -_b / _k; // Predicted maximum velocity
            }
        }
        else
        {
            // v_max ~ mu * sqrt(r / a)
            for (int lane = 0; lane < _trackInfo.numLanes(); ++lane)
            {
                _targetVel[piece][lane] = _mu * sqrt(_trackInfo.laneRadius(piece, lane) / _guideArm);
            }
        }
    }
    _desired = _targetVel[state.piece][state.endLane];
}

double VelocityGovernor::update(CarState const& state, double prevThrottle)
{
    if (!_running)
    {
        return _defaultThrottle;
    }

    _maxAngle = std::max(_maxAngle, std::abs(state.angle / TrackInfo::PI_180));
    if (_maxAngle >= CONFIG_GOVERNOR_ANGLE_UPPER)
    {
        std::cout << "Governor: ATTENTION! Decreasing mu estimate due to excess drifting!" << std::endl;
        std::cout << "    _maxAngle = " << _maxAngle << std::endl;
        _mu -= CONFIG_GOVERNOR_MU_STEP;
        recalculate();
        _maxAngle = 0.0;
    }
    if (_maxAngle < CONFIG_GOVERNOR_ANGLE_LOWER && state.lap > _angleLap)
    {
        std::cout << _maxAngle << std::endl;
        std::cout << "Governor: ATTENTION! Increasing mu estimate due to drifting headroom!" << std::endl;
        std::cout << "    _maxAngle = " << _maxAngle << std::endl;
        _mu += CONFIG_GOVERNOR_MU_STEP;
        recalculate();
        _angleLap = state.lap;
    }

    if (_targetReached)
    {
        //Next piece for which velocity must be governed
        int next = nextChange();

        if (next >= 0)
        {
            double nextDist = distanceTo(state, next);
            double nextVel = _targetVel[next][_route[next]];
            double curVel = _targetVel[state.piece][_route[state.piece]];
            double b = 0.0;

            // Choose throttle parameter depending on the direction of velocity change
            if (nextVel > state.velocity)
            {
                b = _b;
            }
            else
            {
                b = 0.0;
            }
            // Velocity and position equation parameters
            double c1 = state.velocity + b / _k;
            double c2 = -c1 / _k;
            // Time to velocity change
            double tn = std::log(nextVel/c1 + b/(c1 * _k)) / _k;
            // Distance required to apply velocity change
            double rn = (c1 * std::exp(_k * tn) - b * tn) / _k + c2;

            if (rn >= nextDist && nextVel < _desired)
            {
                std::cout << "Governor: target velocity change from " << _desired
                << " to " << nextVel << std::endl;
                _desired = nextVel;
                _desiredPiece = next;
                std::cout << "    rn          = " << rn << std::endl;
                std::cout << "    tn          = " << tn << std::endl;
                std::cout << "    next        = " << next << std::endl;
                std::cout << "    state.piece = " << state.piece << std::endl;
                _targetReached = false;
            }
            else if(std::abs(_desired - curVel) > _deadZone)
            {
                std::cout << "Governor: _desired = " << _desired << "   curVel = " << curVel
                          << std::endl;
                _desired = curVel;
                _desiredPiece = state.piece;
            }
        }
        else
        {
            std::cout << "Governor: WARNING: Couldn't find next velocity change!" << std::endl;
        }
    }

    // Simple P(ID) controller
    double throttle = prevThrottle;
    if (std::abs(_desired - state.velocity) > _deadZone)
    {
        if (state.velocity < _desired)
        {
            throttle = 1.0;
        }
        else
        {
            throttle = 0.0;
        }
    }
    else if (!_targetReached)
    {
        _targetReached = true;
        std::cout << "Governor: velocity target reached" << std::endl;
    }
    if (state.piece == _desiredPiece && !_targetReached)
    {
        _targetReached = true;
        std::cout << "Governor: position target reached" << std::endl;
    }

    return throttle;
}

double VelocityGovernor::distanceTo(CarState const& state, int next) const
{
    // Accommodate for the length left of the current piece
    double dist = -state.inPieceDistance;

    // Sum rest
    int piece = state.piece;
    while (piece != next)
    {
        // Switching?
        int startLane = _route[piece];
        int endLane = _route[(piece + 1) % _trackInfo.numPieces()];
        if (startLane != endLane)
        {
            dist += _trackInfo.switchApprox(piece, startLane, endLane);
        }
        else
        {
            dist += _trackInfo.laneLength(piece, startLane);
        }
        piece = (piece + 1) % _trackInfo.numPieces();
    }
    return dist;
}

int VelocityGovernor::nextChange() const
{
    // Begin search from the piece after current target
    int piece = _desiredPiece + 1;
    while(piece != _desiredPiece)
    {
        //std::cout << "Governor::nextChange(): ";
        //std::cout << "piece: " << piece;
        //std::cout << "   _route[piece]: " << _route[piece];
        //std::cout << std::endl;
        double pieceVel = _targetVel[piece][_route[piece]];
        //double pieceVel = _targetVel[piece][_route[piece]];
        if (std::abs(_desired - pieceVel) > _deadZone)
        {
            return piece;
        }
        piece = (piece + 1) % _trackInfo.numPieces();
    }
    return -1;
}