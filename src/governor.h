#ifndef VEL_MGR_H__
#define VEL_MGR_H__

#include "config.h"
#include "state_PODs.h"

#include <cmath>
#include <vector>

class TrackInfo;

class VelocityGovernor
{
public:
    VelocityGovernor(TrackInfo const& trackInfo):
        _trackInfo(trackInfo),
        _k(CONFIG_PHYSICS_K),
        _b(CONFIG_PHYSICS_B),
        _deadZone(CONFIG_PHYSICS_DEADZONE),
        _guideArm(CONFIG_PHYSICS_ARM),
        _mu(CONFIG_PHYSICS_MU_VEL / std::sqrt(CONFIG_PHYSICS_MU_R / CONFIG_PHYSICS_ARM)),
        _maxAngle(0.0),
        _angleLap(0),
        _desired(-_b/_k),
        _desiredPiece(0),
        _targetReached(true),
        _defaultThrottle(CONFIG_GOVERNOR_DEFAULT_THROTTLE),
        _running(false)
    {}

    void start();
    void stop();

    //double maxVel()
    void readTrack();
    void recalculate(CarState const& state = CarState());
    double update(CarState const& state, double prevThrottle);

    double k() const { return _k; }
    void k(double k_) { _k = k_; }

    double b() const { return _b; }
    void b(double b_) { _b = b_; }

    double deadZone() const { return _deadZone; }
    void deadZone(double deadZone_) { _deadZone = deadZone_; }

    double desired() const { return _desired; }

    double guideArm() const { return _guideArm; }
    void guideArm(double guideArm_) { _guideArm = guideArm_; }

    double mu() const { return _mu; }
    void mu(double mu_) { _mu = mu_; }

    std::vector<int> const& route() const { return _route; }
    void route(std::vector<int> route_) { _route = route_; }

private:
    double distanceTo(CarState const& state, int next) const;
    int nextChange() const;

    TrackInfo const& _trackInfo;
    
    double _k;
    double _b;
    double _deadZone;
    double _guideArm;
    double _mu;

    double _maxAngle;
    int _angleLap;

    // Velocities
    double _desired;
    int _desiredPiece;

    bool _targetReached;

    double _defaultThrottle;
    bool _running;


    std::vector<std::vector<double>> _targetVel;
    std::vector<int> _route;
};

#endif /* end of include guard: VEL_MGR_H__ */