#include "protocol.h"
#include "connection.h"
#include "game_logic.h"

#include <boost/lexical_cast.hpp>

#include <jsoncons/json.hpp>

#include <ctime>
#include <iostream>
#include <string>
#include <sstream>

using namespace hwo_protocol;

void run(hwo_connection& connection, const std::string& name, const std::string& key,
         std::string const& mode, std::string const& trackName,
         std::string const& trackPass, int carCount)
{
    std::ofstream logfile;
    {
        std::ostringstream oss;
        oss << "race-" << time(0) << ".csv";
        logfile.open(oss.str());
        std::cout << "Begin logging into " << oss.str() << std::endl;
    }
    {
        game_logic game(logfile);

        if (mode == "create" && trackName != "" && carCount > 0)
        {
            connection.send_requests({ make_create(name, key, trackName,
                                                 trackPass, carCount) });
        }
        else if (mode == "join" && trackName != "" && carCount > 0)
        {

        }
        else
        {
            connection.send_requests({ make_join_simple(name, key) });
        }

        for (;;)
        {
            boost::system::error_code error;
            auto response = connection.receive_response(error);

            if (error == boost::asio::error::eof)
            {
                std::cout << "Connection closed" << std::endl;
                break;
            }
            else if (error)
            {
                throw boost::system::system_error(error);
            }

            connection.send_requests(game.react(response));
        }
    }
    logfile.close();
}

int main(int argc, const char* argv[])
{
    using boost::lexical_cast;
    try
    {
        if (argc != 6 && argc != 9)
        {
            std::cerr << "Usage: ./run <host> <port> [<botname> {join|join "
                      << "<trackname> <password> <carcount>|create <trackname> "
                      << "<password> <carcount>}]" << std::endl;
            return 1;
        }

        const std::string host(argv[1]);
        const std::string port(argv[2]);
        const std::string name(argv[3]);
        const std::string key(argv[4]);
        const std::string mode(argv[5]);

        std::string trackName = "";
        std::string trackPass = "";
        int carCount = -1;
        if (argc == 9)
        {
            trackName = argv[6];
            trackPass = argv[7];
            carCount = lexical_cast<int>(argv[8]);
        }
        
        std::cout << "Host: " << host << "   port: " << port
                  << "   name: " << name << "   key: " << key
                  << "   mode: " << mode;
        if (argc == 9)
        {
            std::cout << "   track: " << trackName << "   password: " << trackPass
                      << "   carCount: " << carCount;
        }
        std::cout << std::endl;

        hwo_connection connection(host, port);
        run(connection, name, key, mode, trackName, trackPass, carCount);
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        return 2;
    }

    return 0;
}
