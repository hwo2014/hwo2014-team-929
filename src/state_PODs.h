#ifndef STATE_PODS_HEADER_GUARD
#define STATE_PODS_HEADER_GUARD

#include <map>
#include <string>

struct CarState
{
    CarState() :
        lap(-1),
        piece(0),
        radius(0.0),
        curveAngle(0.0),
        startLane(0),
        endLane(0),
        inPieceDistance(0.0),
        deltaDistance(0.0),
        velocity(0.0),
        deltaVelocity(0.0),
        acceleration(0.0),
        angle(0.0)
    {}
    int lap;
    int piece;
    double radius;
    double curveAngle;
    int startLane;
    int endLane;
    double inPieceDistance;
    double deltaDistance;
    double velocity;
    double deltaVelocity;
    double acceleration;
    double angle;
};

struct GameState
{
    GameState() :
        tick(0),
        deltaTick(0),
        throttle(0.0),
        carStates()
    {}
    int tick;
    int deltaTick;
    double throttle;
    std::map<std::string, CarState> carStates;
};

#endif /* end of include guard: STATE_PODS_HEADER_GUARD */