#include "predictor.h"

ParameterPredictor::ParameterPredictor(Pred validator) :
    valid(validator)
{}

ParameterPredictor& ParameterPredictor::operator<<(point_t dataPoint)
{
    if (valid(dataPoint))
    {
        data.push_back(dataPoint);
    }
}

ParameterPredictor::point_t ParameterPredictor::predict()
{
    double N = (double)data.size();
    double sumX = std::accumulate(data.begin(), data.end(), 0.0,
                                  [this] (double sum, point_t point) {
                                      return sum + point.first;
                                  });
    double sumY = std::accumulate(data.begin(), data.end(), 0.0,
                                  [this] (double sum, point_t point) {
                                      return sum + point.second;
                                  });
    double sumX2 = std::accumulate(data.begin(), data.end(), 0.0,
                                   [this] (double sum, point_t point) {
                                       return sum + point.first * point.first;
                                   });
    double sumXY = std::accumulate(data.begin(), data.end(), 0.0,
                                   [this] (double sum, point_t point) {
                                       return sum + point.first * point.second;
                                   });

    double recipD = 1.0 / (N * sumX2 - sumX * sumX);
    _k = recipD * (N * sumXY - sumX * sumY);
    _b = recipD * (sumX2 * sumY - sumX * sumXY);
    return std::make_pair(_k, _b);
}

bool defaultPred(std::pair<double, double>)
{
    return true;
}