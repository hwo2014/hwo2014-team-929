#ifndef CAR_HEADER_GUARD
#define CAR_HEADER_GUARD

#include <jsoncons/json.hpp>

#include <iosfwd>
#include <map>
#include <string>

class Car
{
public:
	Car(std::string name_, std::string color_, double length_, double width_,
		double guidePosition_);

	Car() = default;
	Car(Car const&) = default;
	Car(Car&&) = default;
	Car& operator=(Car const&) = default;
	Car& operator=(Car&&) = default;

	std::string const& name() const { return _name; }
	std::string const& color() const { return _color; }
	double length() const { return _length; }
	double width() const { return _width; }
	double guideArm() const { return _guideArm; }
private:
	std::string _name;
	std::string _color;
	double _length;
	double _width;
	double _guideArm;
};

Car parseCar(const jsoncons::json& carData);

std::map<std::string, Car> parseCarInfo(const jsoncons::json& carsArray);

std::ostream& operator<<(std::ostream& os, Car const& car);

#endif /* end of include guard: CAR_HEADER_GUARD */