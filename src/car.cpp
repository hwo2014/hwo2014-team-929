#include "car.h"

#include <ostream>

Car::Car(std::string name_, std::string color_, double length_, double width_,
		 double guideArm_) :
	_name(name_),
	_color(color_),
	_length(length_),
	_width(width_),
	_guideArm(guideArm_)
{}

Car parseCar(const jsoncons::json& carData)
{
	const auto& id = carData["id"];
	const auto& dims = carData["dimensions"];
	return Car(id["name"].as<std::string>(), id["color"].as<std::string>(),
			   dims["length"].as<double>(), dims["width"].as<double>(),
			   dims["guideFlagPosition"].as<double>());
}

std::map<std::string, Car> parseCarInfo(const jsoncons::json& carsArray)
{
	std::map<std::string, Car> cars;
	for (auto it = carsArray.begin_elements(); it != carsArray.end_elements(); ++it)
    {
    	Car car = parseCar(*it);
        cars[car.color()] = std::move(car);
    }
    return cars;
}

std::ostream& operator<<(std::ostream& os, Car const& car)
{
	os << "Name: " << car.name() << ", Color: " << car.color()
	   << ", Length: " << car.length() << ", Width: " << car.width()
	   << ", Guide position: " << car.guideArm();
	return os;
}
