#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include "car.h"
#include "governor.h"
#include "predictor.h"
#include "pathfinder.h"
#include "state_PODs.h"
#include "track_info.h"

#include <jsoncons/json.hpp>

#include <functional>
#include <iostream>
#include <fstream>
#include <map>
#include <string>
#include <vector>

#define LOG_SEPARATOR ";"

class game_logic
{
public:
    typedef std::vector<jsoncons::json> msg_vector;

    game_logic(std::ofstream& logfile);
    msg_vector react(const jsoncons::json& msg);

private:
    typedef std::function<msg_vector(game_logic*, const jsoncons::json&)> action_fun;

    const std::map<std::string, action_fun> action_map;
    std::ofstream& _logfile;

    TrackInfo _trackInfo;
    std::map<std::string, Car> _cars;

    GameState _currentState;
    std::string _myColor;

    PathFinder _pathFinder;
    VelocityGovernor _governor;
    ParameterPredictor _predictor;

    int _preferredLane;
    std::vector<int> _route;        //Optimal route
    std::vector<int> _startRoute;   //Route from start to optimal
    bool _onRoute;

    bool _predicted;

    msg_vector on_join(const jsoncons::json& data);
    msg_vector on_your_car(const jsoncons::json& data);
    msg_vector on_game_init(const jsoncons::json& data);
    msg_vector on_game_start(const jsoncons::json& data);
    msg_vector on_car_positions(const jsoncons::json& data);
    msg_vector on_crash(const jsoncons::json& data);
    msg_vector on_game_end(const jsoncons::json& data);
    msg_vector on_error(const jsoncons::json& data);

    GameState parseCarPositions(const jsoncons::json& data, int gameTick) const;
    void calculateData(GameState& state);
};

std::ofstream& operator<<(std::ofstream& os, GameState const& state);

std::string makeLogTitle(std::map<std::string, Car> cars);

#endif
