#include "protocol.h"
#include <iostream>

namespace hwo_protocol
{

    jsoncons::json make_request(const std::string& msg_type, const jsoncons::json& data)
    {
        jsoncons::json r;
        r["msgType"] = msg_type;
        r["data"] = data;
        return r;
    }

    jsoncons::json make_request_with_tick(const std::string& msg_type,
                                          const jsoncons::json& data, int tick)
    {
        jsoncons::json r;
        r["msgType"] = msg_type;
        r["data"] = data;
        r["gameTick"] = tick;
        return r;
    }

    jsoncons::json make_join_simple(const std::string& name, const std::string& key)
    {
        jsoncons::json data;
        data["name"] = name;
        data["key"] = key;
        return make_request("join", data);
    }

    jsoncons::json make_create(const std::string& botName, const std::string& botKey,
                               const std::string& trackName, const std::string& password,
                               size_t carCount)
    {
        jsoncons::json botData;
        botData["name"] = botName;
        botData["key"] = botKey;

        jsoncons::json data;
        data["botId"] = botData;
        data["trackName"] = trackName;
        data["password"] = password;
        data["carCount"] = carCount;
        return make_request("createRace", data);
    }

    jsoncons::json make_join(const std::string& botName, const std::string& botKey,
                               const std::string& trackName, const std::string& password,
                               size_t carCount)
    {
        jsoncons::json botData;
        botData["name"] = botName;
        botData["key"] = botKey;

        jsoncons::json data;
        data["botId"] = botData;
        data["trackName"] = trackName;
        data["password"] = password;
        data["carCount"] = carCount;
        return make_request("joinRace", data);
    }

    jsoncons::json make_ping()
    {
        std::cout << "PING" << std::endl;
        return make_request("ping", jsoncons::null_type());
    }

    jsoncons::json make_throttle(double throttle, int tick)
    {
        return make_request_with_tick("throttle", throttle, tick);
    }

}    // namespace hwo_protocol
