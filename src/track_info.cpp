#include "track_info.h"

#include <cmath>
#include <exception>
#include <ostream>
#include <sstream>

TrackPiece::TrackPiece(double length, bool hasSwitch) :
    _angle(0),
    _length(length),
    _radius(1.0/0.0),
    _hasSwitch(hasSwitch),
    _type(Type::Straight)
{}

TrackPiece::TrackPiece(double radius, double angle, bool hasSwitch) :
    _angle(angle),
    _length(std::abs(radius * angle)),
    _radius(radius),
    _hasSwitch(hasSwitch),
    _type(Type::Bend)
{}

TrackPiece parseTrackPiece(const jsoncons::json& pieceData)
{
    if (pieceData.has_member("length"))
    {
        return TrackPiece(pieceData["length"].as<double>(),
                          pieceData.get("switch", false).as<bool>());
    }
    if (pieceData.has_member("radius") && pieceData.has_member("angle"))
    {
        return TrackPiece(pieceData["radius"].as<double>(),
                          pieceData["angle"].as<double>() * TrackInfo::PI_180,
                          pieceData.get("switch", false).as<bool>());
    }

}

std::ostream& operator<<(std::ostream& os, TrackPiece const& piece)
{
    std::string switchText("false");
    if (piece.hasSwitch())
    {
        switchText = "true";
    }

    if (piece.type() == TrackPiece::Type::Straight)
    {
        os << "Type: Straight, Length: " << piece.length()
           << ", hasSwitch: " << switchText;
    }
    else if (piece.type() == TrackPiece::Type::Bend)
    {
        os << "Type: Bend, Angle: " << piece.angle() / TrackInfo::PI_180
           << ", Radius: " << piece.radius()
           << ", Length(center): " << piece.length()
           << ", hasSwitch: " << switchText;
    }
    return os;
}

TrackInfo::TrackInfo(std::string id, std::string name,
                     std::vector<TrackPiece> pieces,
                     std::vector<double> lanePositions) :
    _id(id),
    _name(name),
    _lanePositions(std::move(lanePositions)),
    _pieces(std::move(pieces))
{}

double TrackInfo::laneDistance(int laneIndex1, int laneIndex2) const
{
    return std::abs(_lanePositions.at(laneIndex1) - _lanePositions.at(laneIndex2));
}

double TrackInfo::laneLength(int pieceIndex, int laneIndex) const
{
    const auto& piece = _pieces.at(pieceIndex);
    if (piece.type() == TrackPiece::Type::Straight)
    {
        return piece.length();
    }
    return std::abs(piece.angle()) * laneRadius(pieceIndex, laneIndex);
}

double TrackInfo::laneRadius(int pieceIndex, int laneIndex) const
{
    const auto& piece = _pieces.at(pieceIndex);
    return piece.radius() + std::copysign(1, -piece.angle()) *
           lanePosition(laneIndex);
}

double TrackInfo::centerLength() const
{
    double length = 0.0;
    for (const auto& piece : _pieces)
    {
        length += piece.length();
    }
    return length;
}

double TrackInfo::switchApprox(int piece, int startLane, int endLane) const
{
    if (!_pieces[piece].hasSwitch())
    {
        std::ostringstream oss;
        oss << "Piece #" << piece << " is not a switch!";
        throw std::invalid_argument(oss.str());
    }
    double l1, l2;
    if (_pieces[piece].type() == TrackPiece::Type::Straight)
    {
        // Pythagorean approximation
        l1 = laneDistance(startLane, endLane);
        l2 = laneLength(piece, endLane);
    }
    else
    {
        // Average radius + Pythagorean approximation
        l1 = 0.5 * (laneLength(piece, startLane) + laneLength(piece, endLane));
        l2 = laneDistance(startLane, endLane);
    }
    return std::sqrt(l1 * l1 + l2 * l2);
}

TrackInfo parseTrackInfo(const jsoncons::json& trackData)
{
    const auto& lanesArray = trackData["lanes"];
    const auto& piecesArray = trackData["pieces"];

    std::vector<double> lanes(lanesArray.size());
    std::vector<TrackPiece> pieces;

    for (auto it = lanesArray.begin_elements(); it != lanesArray.end_elements(); ++it)
    {
        lanes[(*it)["index"].as<int>()] = (*it)["distanceFromCenter"].as<double>();
    }

    for (auto it = piecesArray.begin_elements(); it != piecesArray.end_elements(); ++it)
    {
        pieces.push_back(parseTrackPiece(*it));
    }

    return TrackInfo(trackData["id"].as<std::string>(), trackData["name"].as<std::string>(), 
                     std::move(pieces), std::move(lanes));
}

std::ostream& operator<<(std::ostream& os, TrackInfo const& info)
{
    using std::endl;
    os << "Track ID: " << info.id() << endl;
    os << "Track name: " << info.name() << endl;
    os << "Number of lanes: " << info.numLanes() << endl;
    os << "Lane distances from center:" << endl;
    for (int i = 0; i < info.numLanes(); ++i)
    {
        os << "    Lane #" << i << ": " << info.lanePosition(i) << endl;
    }
    os << "Number of pieces: " << info.numPieces() << endl;
    for (int i = 0; i < info.numPieces(); ++i)
    {
        os << "    Piece #" << i << ": " << info.piece(i) << endl;
    }
    return os;
}