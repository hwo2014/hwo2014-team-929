#ifndef PATHFINDER_H__
#define PATHFINDER_H__

#include "dijkstra.h"

class TrackInfo;

class PathFinder
{
public:
	PathFinder(TrackInfo const& trackInfo) :
		_trackInfo(trackInfo)
	{}
	
	void buildGraph();
	double length(int v) const;

	int preferredLane() const;
	std::vector<int> shortestLap(int startLane, int endLane = -1) const;

private:
	TrackInfo const& _trackInfo;

	Dijkstra::adjacency_list_t _adjacencyList;
	std::vector<std::vector<Dijkstra::weight_t>> _distances;
    std::vector<std::vector<Dijkstra::vertex_t>> _paths;
    std::vector<int> _switches;

};

#endif /* end of include guard: PATHFINDER_H__ */