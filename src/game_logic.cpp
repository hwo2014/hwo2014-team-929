#include "game_logic.h"
#include "predictor.h"
#include "protocol.h"
#include "track_info.h"

#include "config.h"

#include <chrono>
#include <string>
#include <sstream>

using namespace hwo_protocol;

bool nonZero(std::pair<double, double> p)
{
    double epsilon = CONFIG_LOG_EPSILON;
    return (std::abs(p.first) > epsilon || std::abs(p.second) > epsilon);
}

game_logic::game_logic(std::ofstream& logfile) :
    action_map{{ "join", &game_logic::on_join },
               { "yourCar", &game_logic::on_your_car },
               { "gameInit", &game_logic::on_game_init },
               { "gameStart", &game_logic::on_game_start },
               { "carPositions", &game_logic::on_car_positions },
               { "crash", &game_logic::on_crash },
               { "gameEnd", &game_logic::on_game_end },
               { "error", &game_logic::on_error }},
    _logfile(logfile),
    _trackInfo(),
    _pathFinder(_trackInfo),
    _governor(_trackInfo),
    _predictor(nonZero),
    _preferredLane(0),
    _route(),
    _startRoute(),
    _onRoute(false),
    _predicted(false)
{
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
    const auto& msg_type = msg["msgType"].as<std::string>();
    const auto& msgData = msg["data"];

    jsoncons::json data;

    if (msg_type == "carPositions")
    {
        data["positions"] = msg["data"];
    }
    else if (msg_type != "gameStart")
    {
        data = msgData;
    }

    if (msg.has_member("gameTick"))
    {
        data["gameTick"] = msg["gameTick"];
    }
    
    auto action_it = action_map.find(msg_type);
    if (action_it != action_map.end())
    {
        return (action_it->second)(this, data);
    }
    else
    {
        std::cout << "Unknown message type: " << msg_type << std::endl;
        return { make_ping() };
    }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
    std::cout << "Joined" << std::endl;
    if (data.has_member("gameTick"))
    {
        return { make_ping() };
    }
    return {};
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
    _myColor = data["color"].as<std::string>();
    std::cout << "My car is " << _myColor << std::endl;
    if (data.has_member("gameTick"))
    {
        return { make_ping() };
    }
    return {};
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
    std::cout << "Game init" << std::endl;
    const auto& raceData = data["race"];

    // Track information
    std::cout << "Parsing track data... ";
    _trackInfo = parseTrackInfo(raceData["track"]);
    std::cout << "Done." << std::endl;
    std::cout << "Track data --------------------------------------------------"
              << std::endl
              << _trackInfo
              << "-------------------------------------------------------------"
              << std::endl;

    // Car information
    std::cout << "Parsing car data... ";
    _cars = parseCarInfo(raceData["cars"]);
    std::cout << "Done." << std::endl;
    std::cout << "Number of cars: " << _cars.size() << std::endl;
    {
        int i = 0;
        for (const auto& item : _cars)
        {
            const auto& car = item.second;
            std::cout << "    Car #" << i << ": " << car << std::endl;
            ++i;
        }
    }

    // Pathfinder
    std::cout << "Initializing pathfinder... " << std::endl;
    std::chrono::high_resolution_clock::time_point
    preInit = std::chrono::high_resolution_clock::now();
    _pathFinder.buildGraph();
    auto duration = std::chrono::high_resolution_clock::now() - preInit;
    std::cout << "    Done in "
              << std::chrono::duration_cast<std::chrono::microseconds>(duration).count()
              << " microseconds." << std::endl;
    for (int i = 0; i < _trackInfo.numLanes(); ++i)
    {
        std::cout << "    Lane #" << i << " shortest lap length: "
                  << _pathFinder.length(i) << std::endl;
    }
    _preferredLane = _pathFinder.preferredLane();
    std::cout << "    Centerline length: " << _trackInfo.centerLength() << std::endl;
    std::cout << "    Preferred lane: " << _preferredLane << ", length: "
              << _pathFinder.length(_preferredLane) << std::endl;
    _route = _pathFinder.shortestLap(_preferredLane);

    // Velocity governor initial guesses
    double initialK = CONFIG_PHYSICS_K;
    double initialB = CONFIG_PHYSICS_B;
    double initialMu = CONFIG_PHYSICS_MU_VEL / std::sqrt(CONFIG_PHYSICS_MU_R / _cars[_myColor].guideArm());
    double initialDead = CONFIG_PHYSICS_DEADZONE;
    std::cout << "Velocity governor initial values:" << std::endl;
    std::cout << "    k        = " << initialK  << std::endl;
    std::cout << "    b        = " << initialB  << std::endl;
    std::cout << "    mu       = " << initialMu << std::endl;
    std::cout << "    guideArm = " << _cars[_myColor].guideArm() << std::endl;
    std::cout << "    deadzone = " << initialDead << std::endl;
    _governor.k(initialK);
    _governor.b(initialB);
    _governor.guideArm(_cars[_myColor].guideArm());
    _governor.mu(initialMu);
    _governor.deadZone(initialDead);
    _governor.route(std::vector<int>(_trackInfo.numPieces(), 0));
    //_governor.route(_route);
    _governor.readTrack();
    _governor.recalculate();
    
    // Log title row
    _logfile << makeLogTitle(_cars);
    _currentState.throttle = 1.0;

    std::cout << "Game init done!" << std::endl;

    if (data.has_member("gameTick"))
    {
        return { make_throttle(_currentState.throttle, data["gameTick"].as<int>()) };
    }
    return {};
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
    std::cout << "Race started" << std::endl;
    
    if (data.has_member("gameTick"))
    {
        return { make_throttle(_currentState.throttle, data["gameTick"].as<int>()) };
    }
    return {};
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
    const auto& positionData = data["positions"];
    int gameTick = data.get("gameTick", -1337).as<int>();

    GameState newState = parseCarPositions(positionData, gameTick);
    const auto& state = _currentState.carStates[_myColor];

    if (gameTick < 0)
    {
        //_startRoute = _pathFinder.shortestLap(state.startLane, _preferredLane);
        //_governor.route(_startRoute);
    }
    else
    {
        // Update to optimal route after first lap
        /*
        if (!_onRoute && state.lap == 1)
        {
            _onRoute = true;
            _governor.route(_route);
            std::cout << "On optimal route!" << std::endl;
        }
        */

        calculateData(newState);
        newState.throttle = _governor.update(state, _currentState.throttle);

        // Predictor run
        if (!_predicted)
        {
            
            if (_predictor.N() < CONFIG_PREDICTOR_N)
            {
                _predictor << std::make_pair(state.velocity, state.acceleration);
            }
            else
            {
                _predictor.predict();
                double k = _predictor.k();
                double b = _predictor.b() / _currentState.throttle;
                std::cout << "Predictor: k = " << k << "   b = " << b << std::endl;
                std::cout << "Maximum velocity: " << -b / k << std::endl;
                _governor.k(k);
                _governor.b(b);
                _governor.recalculate(state);
                _governor.start();
                _predicted = true;
            }
        }

        // Log "lags behind" by one tick
        _logfile << _currentState << std::endl;
        _currentState = std::move(newState);

        const auto& state = _currentState.carStates[_myColor];

        /*
        if (state.lap == 0 && state.piece < 4)
        {
            _currentState.throttle = 1.0;
        }
        if (state.piece >= 34)
        {
            _currentState.throttle = 1.0;
            if (!fullThrottle)
            {
                std::cout << "Full throttle!" << std::endl;
                fullThrottle = true;
            }
        }
        else
        {
            fullThrottle = false;
        }
        if (!zeroThrottle && state.piece == 0 && state.lap > zeroThrottleLap)
        {
            zeroThrottle = true;
            zeroThrottleTick = gameTick;
            zeroThrottleLap = state.lap;
            std::cout << "Zero throttle!" << std::endl;
        }
        if (zeroThrottle)
        {
            _currentState.throttle = 0.0;
            if (gameTick - zeroThrottleTick >= 300)
            {
                zeroThrottle = false;
                std::cout << "Resume throttle." << std::endl;
            }
        }
        */

        return { make_throttle(_currentState.throttle, gameTick) };
    }
    return {};
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
    std::cout << "Someone crashed" << std::endl;
    if (data.has_member("gameTick"))
    {
        return { make_ping() };
    }
    return {};
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
    std::cout << "Race ended" << std::endl;
    if (data.has_member("gameTick"))
    {
        return { make_ping() };
    }
    return {};
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
    std::cout << "Error: " << data.to_string() << std::endl;
    if (data.has_member("gameTick"))
    {
        return { make_ping() };
    }
    return {};
}

GameState game_logic::parseCarPositions(const jsoncons::json& data, int gameTick) const
{
    GameState gameState;
    gameState.tick = gameTick;
    for (auto it = data.begin_elements(); it != data.end_elements(); ++it)
    {
        const auto& piecePosition = (*it)["piecePosition"];
        const auto& laneData = piecePosition["lane"];

        CarState state;
        state.lap = piecePosition["lap"].as<int>();
        state.piece = piecePosition["pieceIndex"].as<int>();
        state.inPieceDistance = piecePosition["inPieceDistance"].as<double>();

        state.startLane = laneData["startLaneIndex"].as<int>();
        state.endLane = laneData["endLaneIndex"].as<int>();

        state.angle = (*it)["angle"].as<double>() * TrackInfo::PI_180;

        gameState.carStates[(*it)["id"]["color"].as<std::string>()] = state;
    }
    return gameState;
}

void game_logic::calculateData(GameState& state)
{
    state.deltaTick = state.tick - _currentState.tick;
    for (auto& item : state.carStates)
    {
        auto& newCarState = item.second;
        auto& curCarState = _currentState.carStates[item.first];

        const auto& newPiece = _trackInfo.piece(newCarState.piece);

        newCarState.curveAngle = newPiece.angle();
        newCarState.radius = _trackInfo.laneRadius(newCarState.piece, newCarState.endLane);
        
        // Check for piece boundary intersect
        if (newCarState.piece == curCarState.piece)
        {
            newCarState.deltaDistance = newCarState.inPieceDistance -
                                        curCarState.inPieceDistance;
        }
        else
        {
            newCarState.deltaDistance = newCarState.inPieceDistance + 
                                        _trackInfo.laneLength(curCarState.piece,
                                                             curCarState.endLane) -
                                        curCarState.inPieceDistance;
        }
        newCarState.velocity = newCarState.deltaDistance / state.deltaTick;

        curCarState.deltaVelocity = newCarState.velocity - curCarState.velocity;
        curCarState.acceleration = curCarState.deltaVelocity / state.deltaTick;
    }
}

std::ofstream& operator<<(std::ofstream& os, GameState const& state)
{
    os << state.tick << LOG_SEPARATOR << state.deltaTick
       << LOG_SEPARATOR << state.throttle << LOG_SEPARATOR;

    for (const auto& item : state.carStates)
    {
        const auto& cs = item.second;
        os << LOG_SEPARATOR << LOG_SEPARATOR << cs.lap << LOG_SEPARATOR
           << cs.piece << LOG_SEPARATOR << cs.radius << LOG_SEPARATOR
           << cs.curveAngle / TrackInfo::PI_180 << LOG_SEPARATOR << cs.startLane << LOG_SEPARATOR
           << cs.endLane << LOG_SEPARATOR << cs.inPieceDistance << LOG_SEPARATOR
           << cs.deltaDistance << LOG_SEPARATOR << cs.velocity << LOG_SEPARATOR
           << cs.deltaVelocity << LOG_SEPARATOR << cs.acceleration << LOG_SEPARATOR
           << cs.angle / TrackInfo::PI_180 << LOG_SEPARATOR;
    }
    return os;
}

std::string makeLogTitle(std::map<std::string, Car> cars)
{
    std::ostringstream oss;
    oss << "\"t\"" << LOG_SEPARATOR "\"delta t\"" << LOG_SEPARATOR
        << "\"throttle\"" << LOG_SEPARATOR;
    for (const auto& item : cars)
    {
        const auto& car = item.second;
        oss << LOG_SEPARATOR << "\"" << car.name() << " - " << car.color()
            << "\"" << LOG_SEPARATOR;
        oss << "\"lap\"" << LOG_SEPARATOR << "\"piece\"" << LOG_SEPARATOR
            << "\"radius\"" << LOG_SEPARATOR << "\"curve a\"" << LOG_SEPARATOR
            << "\"startLane\"" << LOG_SEPARATOR << "\"endLane\""
            << LOG_SEPARATOR << "\"pos\"" << LOG_SEPARATOR
            << "\"delta pos\"" << LOG_SEPARATOR << "\"vel\"" << LOG_SEPARATOR
            << "\"delta vel\"" << LOG_SEPARATOR << "\"acc\"" << LOG_SEPARATOR
            << "\"a\"" << LOG_SEPARATOR;
    }
    oss << std::endl;
    return oss.str();
}