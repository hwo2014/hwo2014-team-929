#ifndef TRACK_INFO_HEADER_GUARD
#define TRACK_INFO_HEADER_GUARD

#include <jsoncons/json.hpp>

#include <iosfwd>
#include <vector>

class TrackPiece
{
public:
    enum class Type {Straight, Bend};

    TrackPiece(double length, bool hasSwitch = false);
    TrackPiece(double radius, double angle, bool hasSwitch = false);

    TrackPiece(TrackPiece const&) = default;
    TrackPiece(TrackPiece&&) = default;
    TrackPiece& operator=(TrackPiece const&) = default;
    TrackPiece& operator=(TrackPiece&&) = default;

    double angle() const { return _angle; }
    double length() const { return _length; }
    double radius() const { return _radius; }

    bool hasSwitch() const { return _hasSwitch; }
    Type type() const { return _type; }
private:
    double _angle;
    double _length;
    double _radius;

    bool _hasSwitch;
    Type _type;
};

TrackPiece parseTrackPiece(const jsoncons::json& pieceData);
std::ostream& operator<<(std::ostream& os, TrackPiece const& piece);

class TrackInfo
{
public:
     static constexpr double PI_180 = 0.01745329251994329576923690768488612713442;

    TrackInfo() = default;
    TrackInfo(std::string id, std::string name,
              std::vector<TrackPiece> pieces,
              std::vector<double> lanePositions);

    TrackInfo(TrackInfo const&) = default;
    TrackInfo(TrackInfo&&) = default;
    TrackInfo& operator=(TrackInfo const&) = default;
    TrackInfo& operator=(TrackInfo&&) = default;

    std::string const& id() const { return _id; }
    std::string const& name() const { return _name; }
    int numPieces() const { return _pieces.size(); }
    int numLanes() const { return _lanePositions.size(); }

    TrackPiece const& piece(int pieceIndex) const { return _pieces.at(pieceIndex); }

    double lanePosition(int laneIndex) const { return _lanePositions.at(laneIndex); }
    double laneDistance(int laneIndex1, int laneIndex2) const;
    double laneLength(int pieceIndex, int laneIndex) const;
    double laneRadius(int pieceIndex, int laneIndex) const;

    double centerLength() const;
    double switchApprox(int piece, int startLane, int endLane) const;

private:
    std::string _id;
    std::string _name;

    std::vector<double> _lanePositions;
    std::vector<TrackPiece> _pieces;
};

TrackInfo parseTrackInfo(const jsoncons::json& trackData);
std::ostream& operator<<(std::ostream& os, TrackInfo const& info);

#endif /* end of include guard: TRACK_INFO_HEADER_GUARD */