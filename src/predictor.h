#ifndef PARAM_PREDICT_H__
#define PARAM_PREDICT_H__

#include <algorithm>
#include <utility>
#include <vector>

bool defaultPred(std::pair<double, double>);

class ParameterPredictor
{
public:
    typedef bool (*Pred)(std::pair<double, double>);
    typedef std::pair<double, double> point_t;

    ParameterPredictor(Pred validator = defaultPred);

    ParameterPredictor& operator<<(point_t dataPoint);

    point_t predict();

    double k() const { return _k; }
    double b() const { return _b; }
    size_t N() const { return data.size(); }

private:
    Pred valid;
    std::vector<point_t> data;
    double _k;
    double _b;
};

#endif /* end of include guard: PARAM_PREDICT_H__ */