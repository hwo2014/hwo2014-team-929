#include "pathfinder.h"

#include "config.h"
#include "track_info.h"

#include <map>

#include <algorithm>
#include <iterator>

#define NDEBUG

#ifndef NDEBUG
    #include <iostream>
    #define DPRINT(a) std::cout << a
#else
    #define DPRINT(a)
#endif

using namespace Dijkstra;

void PathFinder::buildGraph()
{
    _distances.resize(_trackInfo.numLanes());
    _paths.resize(_trackInfo.numLanes());

    for (int i = 0; i < _trackInfo.numPieces(); ++i)
    {
        if (_trackInfo.piece(i).hasSwitch())
        {
            _switches.push_back(i);
        }
    }

    _adjacencyList.resize(_trackInfo.numLanes() * (_switches.size() + 1));
    int prevPiece = 0;

    DPRINT("Switches: "); DPRINT(_switches.size()); DPRINT(std::endl);

    // Iterate over switches
    for (int s = 0; s < _switches.size(); ++s)
    {
        const int switchIndex = _switches[s];
        DPRINT(std::endl);
        DPRINT("Switch #"); DPRINT(s); DPRINT(" - part index: ");
        DPRINT(switchIndex); DPRINT(std::endl);
        const auto& switchPiece = _trackInfo.piece(switchIndex);

        // Iterate over lanes
        for (int lane = 0; lane < _trackInfo.numLanes(); ++lane)
        {
            double length = 0.0;
            //Distance from the end of previous switch the start of the next one
            for (int piece = prevPiece; piece < switchIndex; ++piece)
            {
                length += _trackInfo.laneLength(piece, lane);
            }
            
            // Distances along the switch (left, straight, right)
            for (int target = std::max(0, lane - 1);
                 target <= std::min(_trackInfo.numLanes() - 1, lane + 1); ++target)
            {
                DPRINT("std::max(0, lane - 1): "); DPRINT(std::max(0, lane - 1)); DPRINT(std::endl);
                DPRINT("std::min(_trackInfo.numLanes() - 1, lane + 1): "); DPRINT(std::min(_trackInfo.numLanes() - 1, lane + 1)); DPRINT(std::endl);
                DPRINT("Target: "); DPRINT(target); DPRINT(std::endl);

                double switchLength;
                if (lane != target)
                {
                    switchLength = _trackInfo.switchApprox(switchIndex, lane, target);
                }
                else
                {
                    switchLength = _trackInfo.laneLength(switchIndex, target);
                }

                // On last switch, in case it's not the last piece of the track,
                // sum rest of the track, (ON TARGET LANE!)
                if (s == _switches.size() - 1 && switchIndex == _trackInfo.numPieces() - 1)
                {
                    for (int end = switchIndex + 1; end < _trackInfo.numPieces(); ++end)
                    {
                        switchLength += _trackInfo.laneLength(end, target);
                    }
                }

                DPRINT("Lane "); DPRINT(lane); DPRINT(", Target ");
                DPRINT(target); DPRINT(", Length "); DPRINT(length + switchLength);
                DPRINT(std::endl);

                _adjacencyList[s*_trackInfo.numLanes() + lane].push_back(
                    neighbor((s+1)*_trackInfo.numLanes() + target, length + switchLength));
            }
            
        }
        prevPiece = switchIndex + 1;
    }

    // Walk graphs for each lane
    for (int lane = 0; lane < _trackInfo.numLanes(); ++lane)
    {
        DijkstraComputePaths(lane, _adjacencyList, _distances[lane], _paths[lane]);
    }
}

double PathFinder::length(int v) const
{
    return _distances[v][_adjacencyList.size() - _trackInfo.numLanes() + v];
}

int PathFinder::preferredLane() const
{
    double dist = CONFIG_PATHFINDER_INFINITY;
    int lane = -1;
    for (int i = 0; i < _trackInfo.numLanes(); ++i)
    {
        double curDist = _distances[i][_adjacencyList.size() - _trackInfo.numLanes() + i];
        if (curDist < dist)
        {
            lane = i;
            dist = curDist;
        }
    }
    return lane;
}

std::vector<int> PathFinder::shortestLap(int startLane, int endLane) const
{
    if (endLane == -1)
    {
        endLane = startLane;
    }

    int target = _adjacencyList.size() - _trackInfo.numLanes() + endLane;
    std::cout << "PathFinder::shortestLap(): shortest path from " << startLane << " to "
              << target << ":" << std::endl;

    auto path = DijkstraGetShortestPathTo(target, _paths[startLane]);
    std::copy(path.begin(), path.end(), std::ostream_iterator<vertex_t>(std::cout, " "));
    std::cout << std::endl;

    // Remove root
    path.pop_front();

    std::vector<int> route(_trackInfo.numPieces(), startLane);
    int s = 0;
    std::cout << "switches: " << _switches.size() << std::endl;
    int currentLane = startLane;
    for (int i = 0; i < _trackInfo.numPieces(); ++i)
    {
        if (s < _switches.size() && i == _switches[s])
        {
            std::cout << "Switch #" << s << ", index: " << _switches[s] << std::endl;
            currentLane = path.front() % _trackInfo.numLanes();
            path.pop_front();
            ++s;
        }
        route[i] = currentLane;
    }

    std::cout << "Route vector: ";
    std::copy(route.begin(), route.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;

    return route;
}